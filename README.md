# Descripción
 Es una aplicación web diseñada para realizar procesamientos masivos de secuencias de proteínas utilizando el algoritmo de búsqueda BLASTP. Esta aplicación proporciona una interfaz amigable que permite a los usuarios buscar secuencias de proteínas por su número de acceso en la Base de Datos de Proteínas (PDB) y visualizar los resultados de manera informativa.


# Características
- Realiza búsquedas locales BLASTP utilizando la base de datos PDB.
- Ofrece una API JSON para realizar consultas a través de números de acceso PDB.
- Permite a los usuarios introducir un número de acceso PDB y ver los resultados de la búsqueda BLASTP.
- Admite alineamiento global de dos secuencias de aminoácidos.
- Proporciona la funcionalidad de mostrar un árbol de proximidad para un conjunto de secuencias.


# Uso
Para utilizar la aplicación, simplemente sigue estos pasos:
1. Clona o descarga el repositorio.
2. Instala las dependencias necesarias usando pip install -r requirements.txt.
3. Ejecuta la aplicación con el comando python app.py.
4. Abre tu navegador web y ve a http://localhost:5000/.
5. Introduce el número de acceso PDB en el campo proporcionado y haz clic en "Buscar".
6. Observa los resultados de la búsqueda BLASTP.


# Tecnologías Utilizadas
- Python
- Flask
- HTML
- CSS
- JavaScript
- jQuery