from flask import Flask, request, jsonify, render_template


app = Flask(__name__)

# Función para realizar una búsqueda blastp
def blastp_search(accession_number):
    # Aquí iría la lógica real de búsqueda blastp
    # Esta es solo una función de ejemplo
    result = f"Resultados de búsqueda blastp para {accession_number}"
    return result

# @app.route('/api/blastp', methods=['POST'])
# def perform_blastp_search():
#     data = request.get_json()
#     accession_number = data['accession_number']
#     result = blastp_search(accession_number)
#     return jsonify({'result': result})

@app.route('/', methods=['GET'])
def render_blastp_search_page():
    return render_template('blastp_search.html')

if __name__ == '__main__':
    app.run(debug=True)
